/*
1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
2. Що станеться, якщо в функцію setTimeout() передати нульову затримку?
 Чи спрацює вона миттєво і чому?
3. Чому важливо не забувати викликати функцію clearInterval(), 
коли раніше створений цикл запуску вам вже не потрібен?


1. setTimeout() - викликає функцію через заданий інтервал часу один раз
   setInterval() - регулярно викликає функцію через заданий інтервал часу 

2. функція спрацює миттєво, але після виконання поточного коду

3. Щоб зупинити виконання setInterval() та вигрузити його з пам'яті

*/

const images = document.querySelectorAll(".image-to-show")
const pauseBT = document.getElementById("pauseBT")
const continueBT = document.getElementById("continueBT")


let indexOfImages = 0
const changeImages = () => {    
images[indexOfImages].classList.add("hider")
indexOfImages = (indexOfImages+1)%images.length
images[indexOfImages].classList.remove("hider")
}

let addInterval = setInterval(changeImages, 3000)

let showImages = true
pauseBT.addEventListener("click", () => {
    showImages = false
    clearInterval(addInterval) 
})
continueBT.addEventListener("click", () => {
    showImages = true
    addInterval = setInterval(changeImages, 3000)
})
